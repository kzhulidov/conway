# With OpenMP:  -Xpreprocessor -fopenmp -lomp
conway: conway.c
	$(CC) -Wall -O3 conway.c -o conway -lcurses

clean:
	$(RM) conway

.PHONY: clean
