#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <curses.h>

#define assert_static(e) \
  do { \
    enum { assert_static__ = 1/(e) }; \
  } while (0)

volatile char g_running = 1;

void initial_population(int nr_cells, int max_x, int max_y, char* world)
{
  for (int i = 0; i < nr_cells; i++)
  {
    int x = 1 + rand() % (max_x - 1);
    int y = 1 + rand() % (max_y - 1);

    world[x + y * max_x] = 1;
  }
}

void blinker(int x, int y, int max_x, int max_y, char* world)
{
  world[x - 1 + y * max_x] = 1;
  world[x + y * max_x] = 1;
  world[x + 1 + y * max_x] = 1;
}

int count_neighbors(int x, int y, int max_x, int max_y, char* world)
{
  const int num_cells = 8;
  const static int x_coord[] = {-1, 0, 1, 1, 1, 0, -1, -1};
  const static int y_coord[] = {-1, -1, -1, 0, 1, 1, 1, 0};
  assert_static(sizeof(x_coord)/sizeof(x_coord[0]) == num_cells);
  assert_static(sizeof(y_coord)/sizeof(y_coord[0]) == num_cells);
  int neighbors = 0;

  for (int i = 0; i < num_cells; i++)
  {
    neighbors += world[x + x_coord[i] + (y + y_coord[i]) * max_x];
  }

  return neighbors;
}

void evolve(int max_x, int max_y, char** world1, char** world2)
{
  for (int y = 1; y < max_y; y++)
  {
    for (int x = 1; x < max_x; x++)
    {
      char* cell = &((*world2)[x + y * max_x]);
      int neighbors = count_neighbors(x, y, max_x, max_y, *world1);

      if ((neighbors < 2) || (neighbors > 3))
      {
        *cell = 0;
      }
      else if (neighbors == 3)
      {
        *cell = 1;
      }
      else
      {
        *cell = (*world1)[x + y * max_x];
      }
    }
  }

  char* tmp = *world2;
  *world2 = *world1;
  *world1 = tmp;
}

void draw(int max_x, int max_y, char* world)
{
  for (int y = 1; y < max_y; y++)
  {
    for (int x = 1; x < max_x; x++)
    {
      mvaddch(y, x, world[x + y * max_x] ? 'o' : ' ');
    }
  }
  
  refresh();
}

void finish(int sig)
{
  g_running = 0;
}

int main()
{
  signal(SIGINT, finish);

  WINDOW* wnd = initscr();
  nonl();
  cbreak();
 
  int max_x = 0;
  int max_y = 0;
  getmaxyx(wnd, max_y, max_x);
  
  const size_t world_size = max_x * max_y * sizeof(char);
  char* world1 = malloc(world_size);
  char* world2 = malloc(world_size);
  memset(world1, 0, world_size);

  initial_population(max_x * max_y / 3, max_x, max_y, world1);
  //blinker(max_x / 2, max_y / 2, max_x, max_y, world1);
  draw(max_x, max_y, world1);

  for (unsigned int iteration = 0; g_running && (iteration < 1000000); iteration++)
  {
    evolve(max_x, max_y, &world1, &world2);
    draw(max_x, max_y, world1);
    usleep(100000);
  }

  free(world1);
  free(world2);

  endwin();
  return 0;
}